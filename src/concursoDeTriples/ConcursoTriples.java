package concursoDeTriples;

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Kath
 */
public class ConcursoTriples {

    private int jugador1, jugador2, cont, cont2, puntos, puntos2, desempate, desempate2;
    private int[] estante1;
    private int[] estante2;
    private int[] estante3;

    public ConcursoTriples() {
        this.estante1 = new int[5];
        this.estante2 = new int[5];
        this.estante3 = new int[5];

    }

    public void asignarDatos() {
        //Posiciones de cada estación
        this.estante1[0] = 1;
        this.estante1[1] = 2;
        this.estante1[2] = 3;
        this.estante1[3] = 4;
        this.estante1[4] = 5;

        this.estante2[0] = 1;
        this.estante2[1] = 2;
        this.estante2[2] = 3;
        this.estante2[3] = 4;
        this.estante2[4] = 5;

        this.estante3[0] = 1;
        this.estante3[1] = 2;
        this.estante3[2] = 3;
        this.estante3[3] = 4;
        this.estante3[4] = 5;

    }

    public void jugador1(int encestado) {
        cont += 1;
        desempate += 1;
        if (cont == 1) {
            if (encestado == estante1[0]) {
                puntos += 1;
            }
        }
        if (cont == 2) {
            if (encestado == estante1[1]) {
                puntos += 1;
            }
        }
        if (cont == 3) {
            if (encestado == estante1[2]) {
                puntos += 1;
            }
        }
        if (cont == 4) {
            if (encestado == estante1[3]) {
                puntos += 1;
            }
        }
        if (cont == 5) {
            if (encestado == estante1[4]) {
                puntos += 2;
            }
        }
        if (cont == 6) {
            if (encestado == estante2[0]) {
                puntos += 1;
            }
        }
        if (cont == 7) {
            if (encestado == estante2[1]) {
                puntos += 1;
            }
        }
        if (cont == 8) {
            if (encestado == estante2[2]) {
                puntos += 2;
            }
        }
        if (cont == 9) {
            if (encestado == estante2[3]) {
                puntos += 1;
            }
        }
        if (cont == 10) {
            if (encestado == estante2[4]) {
                puntos += 2;
            }
        }
        if (cont == 11) {
            if (encestado == estante3[0]) {
                puntos += 2;
                desempate += 1;
            }
        }
        if (cont == 12) {
            if (encestado == estante3[1]) {
                puntos += 2;
                desempate += 1;

            }
        }
        if (cont == 13) {
            if (encestado == estante3[2]) {
                puntos += 2;
                desempate += 1;

            }
        }
        if (cont == 14) {
            if (encestado == estante3[3]) {
                puntos += 2;
                desempate += 1;

            }
        }
        if (cont == 15) {
            if (encestado == estante3[4]) {
                puntos += 2;
                desempate += 1;

            }
        }
        if (cont > 15) {
            System.out.println("Ya ha realizado todos sus lanzaminetos, es turno solo del jugador 2.");
        }

    }

    public void jugador2(int encestado) {
        cont2 += 1;
        desempate2 += 1;
        if (cont2 == 1) {
            if (encestado == estante1[0]) {
                puntos2 += 1;
            }
        }
        if (cont2 == 2) {
            if (encestado == estante1[1]) {
                puntos2 += 1;
            }
        }
        if (cont2 == 3) {
            if (encestado == estante1[2]) {
                puntos2 += 1;
            }
        }
        if (cont2 == 4) {
            if (encestado == estante1[3]) {
                puntos2 += 1;
            }
        }
        if (cont2 == 5) {
            if (encestado == estante1[4]) {
                puntos2 += 2;
            }
        }
        if (cont2 == 6) {
            if (encestado == estante2[0]) {
                puntos2 += 1;
            }
        }
        if (cont2 == 7) {
            if (encestado == estante2[1]) {
                puntos2 += 1;
            }
        }
        if (cont2 == 8) {
            if (encestado == estante2[2]) {
                puntos2 += 2;
            }
        }
        if (cont2 == 9) {
            if (encestado == estante2[3]) {
                puntos2 += 1;
            }
        }
        if (cont2 == 10) {
            if (encestado == estante2[4]) {
                puntos2 += 2;
            }
        }
        if (cont2 == 11) {
            if (encestado == estante3[0]) {
                puntos2 += 2;
                desempate2 += 1;
            }
        }
        if (cont2 == 12) {
            if (encestado == estante3[1]) {
                puntos2 += 2;
                desempate2 += 1;
            }
        }
        if (cont2 == 13) {
            if (encestado == estante3[2]) {
                puntos2 += 2;
                desempate2 += 1;
            }
        }
        if (cont2 == 14) {
            if (encestado == estante3[3]) {
                puntos2 += 2;
                desempate2 += 1;
            }
        }
        if (cont2 == 15) {
            if (encestado == estante3[4]) {
                puntos2 += 2;
                desempate2 += 1;
            }
        }
        if (cont2 == 15) {
            System.out.println("Ya ha realizado todos sus lanzaminetos, es turno solo del jugador 1.");
        }
    }

    public String cantTiros() {
        String estado = "";
        if (cont > 15 && cont2 > 15) {
            estado = "Los dos jugadores han realizado todos los lanzamientos. Dirijase a ver los resultados.";
        }
        return estado;
    }

    public String resultadoFinal() {
        String resultado = "";
        int calcResult = 0;
        if (cont < 15 || cont2 < 15) {
            resultado = "No han terminado todos los tiros. Continúen jugando por favor.";
        } else {
            resultado = "Puntos del jugador 1: " + puntos
                    + "\nPuntos del jugador 2: " + puntos2;
            if (puntos > puntos2) {
                resultado += "\nEl ganador es el jugador 1";
            } else {
                resultado += "\nEl ganador es el jugador 2";
            }
            if (puntos == puntos2) {
                resultado = "\nHAY UN EMPATE.";
                if (desempate > desempate2) {
                    resultado += "\nSegún los tiros en el estante #3 el ganador es el jugador 1!!";
                } else {
                    resultado += "\nSegún los tiros en el estante #3 el ganador es el jugador 2!!";

                }
            }
        }
        return resultado;
    }
}
