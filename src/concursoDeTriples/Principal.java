/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concursoDeTriples;

import java.util.Random;

/**
 *
 * @author Kath
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int numeroAdivinar;

        ConcursoTriples concurso = new ConcursoTriples();

        concurso.asignarDatos();

        boolean continuar = true;

        do {
            int opcion = menu();
            switch (opcion) {
                case 1:
                    Random oRandom = new Random();
                    numeroAdivinar = oRandom.nextInt(5);
                    concurso.jugador1(numeroAdivinar);
                    System.out.println(concurso.cantTiros());
                    break;
                case 2:
                    oRandom = new Random();
                    numeroAdivinar = oRandom.nextInt(5);
                    concurso.jugador2(numeroAdivinar);
                    System.out.println(concurso.cantTiros());
                    break;
                case 3:
                    System.out.println("Resultados");
                    System.out.println(concurso.resultadoFinal());
                    break;
                case 4:
                    continuar = false;
                    break;
            }
        } while (continuar);
    }

    public static int menu() {
        int opcion = 0;
        System.out.println("Bienvenido al Concurso de Triples");
        System.out.println(" 1 - Tiro de Jugador 1");
        System.out.println(" 2 - Tiro de Jugador 2.");
        System.out.println(" 3 - Resultados");
        System.out.println(" 4 - Salir.");
        opcion = Utilitario.capturaValorEntero("DIGITE UNA OPCION:", 1, 4);
        return opcion;
    }

}
